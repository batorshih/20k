from django.contrib import admin
from .models import Schedule, Project, Equipment, ServiceOther
# Register your models here.
admin.site.register(Schedule)
admin.site.register(Project)
admin.site.register(Equipment)
admin.site.register(ServiceOther)