from django.shortcuts import render
from django.forms import formset_factory,inlineformset_factory
from django.views.generic import CreateView, UpdateView ,ListView
from .models import Schedule, Equipment, Project
from .forms import *
from django.db import transaction
from django.urls import reverse_lazy
from django.shortcuts import redirect

import datetime

#task 32
def edit(request):

    a = Schedule.objects.get(pk=1)
    x = datetime.datetime.now()
    a.is_open(x)
    if request.method == 'POST':
        form = ScheduleForm(request.POST,instance=a)
        if form.is_valid():
            u = form.save()
    else:
        form = ScheduleForm(instance=a)

    return render(request, 'edit.html',{'form':form})


#39 Тоног төхөөрөмж бүртгэх
class ProjectList(ListView):
    model = Project
    template_name = 'project_list.html'

#39 Тоног төхөөрөмж бүртгэх
class ProjectCreate(CreateView):
    model = Project
    fields = '__all__'
    template_name = 'project_form.html'
    success_url = reverse_lazy('project-list')

    def get_context_data(self, **kwargs):
        data = super(ProjectCreate, self).get_context_data(**kwargs)
        formset = inlineformset_factory(Project, Equipment, form=EquipmentForm1, extra=1)
        if self.request.POST:
            data['equipments'] = formset(self.request.POST)
        else:
            data['equipments'] = formset()

        return data

    def form_valid(self, form):
        context = self.get_context_data()
        equipments = context['equipments']
        with transaction.atomic():
            self.object = form.save()
            if equipments.is_valid():
                equipments.instance = self.object
                equipments.save()
        return super(ProjectCreate, self).form_valid(form)


#45 Үйл ажиллагааны төрөл бүртгэх
def add(request):
    id = 9
    if id:
        project = Project.objects.get(pk=id)  # if this is an edit form, replace the author instance with the existing one
    else:
        project = Project()
    project_form = ProjectForm(instance=project) # setup a form for the parent

    InlineFormSet = inlineformset_factory(Project, ServiceOther, form=ServiceOtherForm, extra=1)
    formset = InlineFormSet(instance=project)

    if request.method == "POST":
        project_form = ProjectForm(request.POST)

        if id:
            project_form = ProjectForm(request.POST, instance=project)

        formset = InlineFormSet(request.POST)

        if project_form.is_valid():
            created_project = project_form.save(commit=False)
            formset = InlineFormSet(request.POST, instance=created_project)

            if formset.is_valid():
                created_project.save()
                formset.save()
                return redirect('/')
    context = {}
    context['project_form'] = project_form
    context['formset'] = formset
    return render(request, "add.html", context)
