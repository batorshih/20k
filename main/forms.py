from django.forms import ModelForm
from .models import Schedule,Equipment,ServiceOther,Project
from django import forms


class ScheduleForm(ModelForm):

    # def clean(self):
    #     cleaned_data = super().clean()
    #     print(cleaned_data)
    #     print(cleaned_data['day1'])
    #     if self.errors:
    #         print(self.errors)
    #
    #         print('error')
    #     #    return cleaned_data
    #     return cleaned_data
    class Meta:
         model = Schedule
         fields = '__all__'
         widgets = {
             'day1_open': forms.TimeInput(format='%H:%M'),
             'day1_close': forms.TimeInput(format='%H:%M'),
             'day2_open': forms.TimeInput(format='%H:%M'),
             'day2_close': forms.TimeInput(format='%H:%M'),
             'day3_open': forms.TimeInput(format='%H:%M'),
             'day3_close': forms.TimeInput(format='%H:%M'),
             'day4_open': forms.TimeInput(format='%H:%M'),
             'day4_close': forms.TimeInput(format='%H:%M'),
             'day5_open': forms.TimeInput(format='%H:%M'),
             'day5_close': forms.TimeInput(format='%H:%M'),
             'day6_open': forms.TimeInput(format='%H:%M'),
             'day6_close': forms.TimeInput(format='%H:%M'),
             'day7_open': forms.TimeInput(format='%H:%M'),
             'day7_close': forms.TimeInput(format='%H:%M'),
         }

    def __init__(self, *args, **kwargs):
        super(ScheduleForm, self).__init__(*args, **kwargs)
        self.fields['day1'].widget.attrs['class'] = 'dj_check'
        self.fields['day2'].widget.attrs['class'] = 'dj_check'
        self.fields['day3'].widget.attrs['class'] = 'dj_check'
        self.fields['day4'].widget.attrs['class'] = 'dj_check'
        self.fields['day5'].widget.attrs['class'] = 'dj_check'
        self.fields['day6'].widget.attrs['class'] = 'dj_check'
        self.fields['day7'].widget.attrs['class'] = 'dj_check'


class EquipmentForm1(ModelForm):
    class Meta:
        model = Equipment
        # exclude = ('kind',)
        fields = '__all__'
        widgets = {
            'specs': forms.Textarea(attrs={'rows': 4, 'cols': 20}),
            'kind': forms.HiddenInput()
        }

    def __init__(self, *args, **kwargs):
        super(EquipmentForm1, self).__init__(*args, **kwargs)
        self.fields['cost'].widget.attrs['class'] = 'sum_0'
        self.fields['cost_lease'].widget.attrs['class'] = 'sum_1'
        self.fields['kind'].initial = 'mobile'


class EquipmentForm2(ModelForm):
    class Meta:
        model = Equipment
        fields = '__all__'
        widgets = {
            'specs': forms.Textarea(attrs={'rows': 4, 'cols': 20}),
            'kind': forms.HiddenInput()
        }

    def __init__(self, *args, **kwargs):
        super(EquipmentForm2, self).__init__(*args, **kwargs)
        self.fields['cost'].widget.attrs['class'] = 'sum_0'
        self.fields['cost_lease'].widget.attrs['class'] = 'sum_1'
        self.fields['kind'].initial = 'fixed'


class EquipmentForm3(ModelForm):
    class Meta:
        model = Equipment
        fields = '__all__'
        widgets = {
            'specs': forms.Textarea(attrs={'rows': 4, 'cols': 20}),
            'kind': forms.HiddenInput()
        }

    def __init__(self, *args, **kwargs):
        super(EquipmentForm3, self).__init__(*args, **kwargs)
        self.fields['cost'].widget.attrs['class'] = 'sum_0'
        self.fields['cost_lease'].widget.attrs['class'] = 'sum_1'
        self.fields['kind'].initial = 'small'


class ProjectForm(ModelForm):
    class Meta:
        model = Project
        fields = '__all__'


class ServiceOtherForm(ModelForm):
    class Meta:
        model = ServiceOther
        fields = '__all__'
