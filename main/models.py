from django.db import models as m
from django.db import models
# Create your models here.


class Schedule(m.Model):
    day1_open = m.TimeField(null=True, blank=True)
    day2_open = m.TimeField(null=True, blank=True)
    day3_open = m.TimeField(null=True, blank=True)
    day4_open = m.TimeField(null=True, blank=True)
    day5_open = m.TimeField(null=True, blank=True)
    day6_open = m.TimeField(null=True, blank=True)
    day7_open = m.TimeField(null=True, blank=True)

    day1_close = m.TimeField(null=True, blank=True)
    day2_close = m.TimeField(null=True, blank=True)
    day3_close = m.TimeField(null=True, blank=True)
    day4_close = m.TimeField(null=True, blank=True)
    day5_close = m.TimeField(null=True, blank=True)
    day6_close = m.TimeField(null=True, blank=True)
    day7_close = m.TimeField(null=True, blank=True)

    day1 = m.BooleanField(default=False)
    day2 = m.BooleanField(default=False)
    day3 = m.BooleanField(default=False)
    day4 = m.BooleanField(default=False)
    day5 = m.BooleanField(default=False)
    day6 = m.BooleanField(default=False)
    day7 = m.BooleanField(default=False)

    def is_open(self, datetime_check):
        day_of_week = datetime_check.weekday()
        only_time = datetime_check.time()

        if day_of_week == 0 and not self.day1:
            return self.day1_open <= only_time <= self.day1_close
        if day_of_week == 1 and not self.day2:
            return self.day2_open <= only_time <= self.day2_close
        if day_of_week == 2 and not self.day3:
            return self.day3_open <= only_time <= self.day3_close
        if day_of_week == 3 and not self.day4:
            return self.day4_open <= only_time <= self.day4_close
        if day_of_week == 4 and not self.day5:
            return self.day5_open <= only_time <= self.day5_close
        if day_of_week == 5 and not self.day6:
            return self.day6_open <= only_time <= self.day6_close
        if day_of_week == 6 and not self.day7:
            return self.day7_open <= only_time <= self.day7_close

        return False


class Project(models.Model):
    name = models.CharField(max_length=250)


class Equipment(models.Model):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)

    # Тоног төхөөрөмжийн төрөл
    KIND_CHOICES = [
        ('mobile', 'Нүүлгэж болохуйц том тоног төхөөрөмж'),
        ('fixed', 'Суурийн тоног төхөөрөмж'),
        ('small', 'Жижиг оврын нүүлгэж болохуйц тоног төхөөрөмж'),
    ]
    kind = models.CharField(max_length=20, choices=KIND_CHOICES,verbose_name='төрөл')

    # Тоног төхөөрөмжийн нэр
    name = models.CharField(max_length=250,verbose_name='тоног төхөөрөмжийн нэр')

    # Тоо ширхэг
    amount = models.PositiveIntegerField(verbose_name='тоо ширхэг')

    # Үзүүлэлт
    specs = models.TextField(verbose_name='Үзүүлэлт')

    # Нэмж авсан / сольсон
    ACTION_CHOICES = [
        ('new', 'Нэмж авсан'),
        ('replaced', 'Сольсон'),
    ]
    action = models.CharField(max_length=20, choices=ACTION_CHOICES, verbose_name='нэмж авсан солисон')

    # Худалдан авсан / Түрээс / Хандив
    FUNDING_CHOICES = [
        ('bought', 'Худалдан авсан'),
        ('lease', 'Түрээс'),
        ('donation', 'Хандив'),
    ]
    funding = models.CharField(max_length=20, choices=FUNDING_CHOICES,verbose_name='Худалдан авсан / Түрээс / Хандив')

    # Нийт худалдан авсан өртөг / Хандивийн өртөг
    cost = models.PositiveIntegerField(null=True,verbose_name='Нийт худалдан авсан өртөг / Хандивийн өртөг')

    # Түрээсийн үнэ
    cost_lease = models.PositiveIntegerField(null=True,verbose_name='# Түрээсийн үнэ')


class ServiceOther(models.Model):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)

    # Эрхлэх үйл ажиллагааны төрөл
    name = models.CharField(max_length=250, verbose_name='Эрхлэх үйл ажиллагааны төрөл')

    # Одоогийн тусгай зөвшөөрлөөр эрхэлж буй
    current = models.CharField(max_length=250,verbose_name='Одоогийн тусгай зөвшөөрлөөр эрхэлж буй')

    # Шинээр санал болгож байгаа
    proposed = models.CharField(max_length=250,verbose_name='Шинээр санал болгож байгаа')

    # Төсөл хэрэгжиж дууссаны дараах
    final = models.CharField(max_length=250,verbose_name='Төсөл хэрэгжиж дууссаны дараах')